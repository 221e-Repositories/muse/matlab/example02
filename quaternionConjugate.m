function qconj = quaternionConjugate(p)
% pq = quaternionProduct(p,q)
% 
% Perform product between quaternions.
%
% Input: 
%   - quaternion, p
%   - quaternion, q
% Output: resultant product quaternion, pq
% 
% Author: Daniele Comotti - daniele.comotti@221e.com
%         Roberto Bortoletto - roberto.bortoletto@221e.com
% 
% 221e srl, info@221e.it
% 
% Copyright (c) 2018 All Rights Reserved by 221e srl
    
    %% Extract quaternion components
    p0 = p(1); p1 = p(2); p2 = p(3); p3 = p(4);

    %% Perform multiplication
    qconj = [p0 -p1 -p2 -p3];
end